# ES7 + Sass

A simple gulp-based boilerplate with Babel for ES7 (async/await included)
and Stylus (allows importing css files as well).

Look for `.builder/config.js` for configuring locations.

Use `yarn dev` or `npm run dev` to start watching.
