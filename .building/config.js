import path from 'path'

export const ROOT = path.resolve(__dirname, '..')

// -- STYLUS --------------------------------------------------------------------------

export const CSS_DIR = path.join(ROOT, 'assets', 'css')

export const STYLUS_DIR = path.join(ROOT, 'src', 'stylus')
export const STYLUS_FILE = path.join(STYLUS_DIR, 'styles.styl')

export const WATCH_STYLUS = [ path.join(STYLUS_DIR, '**', '*.styl') ]

// -- JS ----------------------------------------------------------------------------

export const JS_SRC_DIR = path.join(ROOT, 'src', 'js')
export const JS_SRC_FILE = path.join(JS_SRC_DIR, 'app.js')

export const JS_DEST_DIR = path.join(ROOT, 'assets', 'js')
export const JS_DEST_FILE = 'script.js'

export const WATCH_JS = [ path.join(JS_SRC_DIR, '**', '*.js') ]