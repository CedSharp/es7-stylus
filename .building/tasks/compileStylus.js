import { src, dest } from 'gulp'
import rename from 'gulp-rename'
import stylus from 'gulp-stylus'
import { STYLUS_FILE, CSS_DIR } from '../config'

const compileStylus = () => new Promise((resolve, reject) => {
  return src(STYLUS_FILE)
    .pipe(stylus({ 'include css': true }))
    .on('error', reject)
    .pipe(rename('styles.css'))
    .pipe(dest(CSS_DIR))
    .on('end', resolve)
})

export default compileStylus