import { src, dest } from 'gulp'
import source from 'vinyl-source-stream'
import buffer from 'vinyl-buffer'
import browserify from 'browserify'
import tap from 'gulp-tap'
import rename from 'gulp-rename'
import { log, task } from '../logger'
import { JS_SRC_FILE, JS_DEST_DIR, JS_DEST_FILE } from '../config'

const bundlerConfig = {
  entries: [ JS_SRC_FILE ],
  debug: true,
  extensions: [' ', '.js']
}

const compileJs = async () => new Promise((resolve, reject) => {
  src(JS_SRC_FILE, { read: false })
    .pipe(tap(file => {
      file.contents = browserify(file.path, bundlerConfig)
        .transform("babelify", { presets: ["@babel/preset-env"], plugins: ["@babel/plugin-transform-runtime"] })
        .bundle()
    }))
    .on('error', reject)
    .pipe(buffer())
    .pipe(rename(JS_DEST_FILE))
    .pipe(dest(JS_DEST_DIR))
    .on('end', resolve)
})

export default compileJs
