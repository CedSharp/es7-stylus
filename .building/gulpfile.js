// NPM Imports
import { error, log, task, warn } from './logger'
import { src, dest, watch, series, parallel } from 'gulp'

// Config
import { WATCH_STYLUS, WATCH_JS } from './config'

// Tasks
import compileStylus from './tasks/compileStylus'
import compileJs from './tasks/compileJs'

export const watchStylus = async () => {
  await compileStylus()
  task('STYLUS', 'Compiled CSS')

  task('STYLUS', 'Watching files for changes...')
  return watch(WATCH_STYLUS, async done => {
    task('STYLUS', 'Recompiling CSS...', false)
    await compileStylus()
    log(' DONE!')
    done()
  })
}

export const watchJs = async () => {
  await compileJs()
  task('BABEL', 'Compiled JS')

  task('BABEL', 'Watching files for changes...')
  return watch(WATCH_JS, async done => {
    task('BABEL', 'Recompiling JS...', false)
    await compileJs()
    log(' DONE!')
    done()
  })
}

export default parallel([ watchStylus, watchJs ])